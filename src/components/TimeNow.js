import { Component, useInsertionEffect } from "react";

class TimeNow extends Component {
    constructor(props){
        super(props)
        this.state = {
            color: "black"
        }
    }

    onBtnChangeColor = () => {
        const seconds = new Date().getSeconds();
        if(seconds % 2) {
            this.setState({
                color: "red"
            })
        } else {
            this.setState({
                color: "green"
            })
        }
    }

    render () {
        const time = new Date().toLocaleTimeString();
        return(
            <>
            <h1>Hello World!</h1>
            <h3 style={{color: this.state.color}}>It is {time}</h3>
            <button onClick={this.onBtnChangeColor}>Change Color</button>
            </>
        )
    }
}
export default TimeNow;
